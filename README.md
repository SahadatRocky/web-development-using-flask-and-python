# Web Development Using Flask and Python with SQLAlchemy 
#Envirenment Create

- pip install virtualenv
- virtualenv env

#powershell env activate

- .\env\Scripts\activate.ps1

#Installation:

- pip install Flask
- pip install Flask-MySQLdb
- pip install Flask-SQLAlchemy
- pip install Jinja2==2.10.3

#run app.py file

- python app.py
