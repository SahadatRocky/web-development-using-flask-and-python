from flask import Flask,render_template,request,session,redirect
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
import os
import math

from werkzeug.utils import secure_filename
import json
app = Flask(__name__)

with open("static/config.json", 'r') as c:
    params = json.load(c)["params"]

local_server = params['local_server']
app.secret_key = "Secret Key"
app.config['UPLOAD_FOLDER'] = params['upload_location']
# SqlAlchemy Database Configuration With Mysql
if (local_server):
    app.config['SQLALCHEMY_DATABASE_URI'] = params['local_uri']
else:
    app.config['SQLALCHEMY_DATABASE_URI'] = params['prod_uri']

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


db = SQLAlchemy(app)
# Creating model table for our CRUD database
class Contacts(db.Model):
    __tablename__ = "contacts"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable= False)
    email = db.Column(db.String(100), nullable= False)
    phone = db.Column(db.String(100), nullable= False)
    msg = db.Column(db.String(100), nullable= False)
    date = db.Column(db.String(12), nullable=True)

    def __init__(self, name, email, phone, msg, date):
        self.name = name
        self.email = email
        self.phone = phone
        self.msg = msg
        self.date = date


class Posts(db.Model):
    __tablename__ = "posts"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable= False)
    content = db.Column(db.String(100), nullable= False)
    slag = db.Column(db.String(100), nullable= False)
    img_file = db.Column(db.String(100), nullable= False)
    subtitle = db.Column(db.String(100), nullable= False)
    date = db.Column(db.String(12), nullable=True)

    def __init__(self, title, content, slag, img_file,subtitle, date):
        self.title = title
        self.content = content
        self.slag = slag
        self.img_file = img_file
        self.subtitle = subtitle
        self.date = date



@app.route("/")
def home():
    posts = Posts.query.filter_by().all()
    last = math.ceil(len(posts) / int(params['no_of_posts']))
    page = request.args.get('page')
    if (not str(page).isnumeric()):
        page = 1
    page = int(page)
    posts = posts[(page - 1) * int(params['no_of_posts']):(page - 1) * int(params['no_of_posts']) + int(params['no_of_posts'])]
    if page == 1:
        prev = "#"
        next = "/?page=" + str(page + 1)
    elif page == last:
        prev = "/?page=" + str(page - 1)
        next = "#"
    else:
        prev = "/?page=" + str(page - 1)
        next = "/?page=" + str(page + 1)

    return render_template('index.html', params=params, posts=posts, prev=prev, next=next)

@app.route("/edit/<string:id>" , methods=['GET', 'POST'])
def edit(id):

    if 'user' in session and session['user'] == params['username'] :

        if request.method == "POST":
            box_title = request.form.get('title')
            sline = request.form.get('subtitle')
            slag = request.form.get('slag')
            content = request.form.get('content')
            img_file = request.form.get('img_file')
            date = datetime.now()

            print("edit------->>>>")
            post = Posts.query.filter_by(id=id).first()
            post.title = box_title
            post.subtitle = sline
            post.content = content
            post.slag = slag
            post.img_file = img_file
            post.date = date
            db.session.commit()
            return redirect("/edit/" + id)


    post = Posts.query.filter_by(id=id).first()
    return render_template('edit.html', params=params, post=post)
@app.route("/delete/<string:id>" , methods=['GET', 'POST'])
def delete(id):
    if 'user' in session and session['user'] == params['username']:
        post = Posts.query.filter_by(id=id).first()
        db.session.delete(post)
        db.session.commit()
    return redirect("/dashboard")


@app.route("/add/<string:id>" , methods=['GET', 'POST'])
def add(id):

    if 'user' in session and session['user'] == params['username'] :

        if request.method == "POST":
            box_title = request.form.get('title')
            sline = request.form.get('subtitle')
            slag = request.form.get('slag')
            content = request.form.get('content')
            img_file = request.form.get('img_file')
            date = datetime.now()

            if id == '0':
                print("add------->>>>")
                post = Posts(title=box_title, content=content, slag=slag, img_file=img_file, subtitle=sline, date=date)
                db.session.add(post)
                db.session.commit()


    post = Posts.query.filter_by(id=id).first()
    return render_template('add.html', params=params,post=post, id=id)

@app.route("/uploader" , methods=['GET', 'POST'])
def uploader():
    if "user" in session and session['user']==params['username']:
        if request.method=='POST':
            f = request.files['file1']
            f.save(os.path.join(app.config['UPLOAD_FOLDER'], secure_filename(f.filename)))
            return "Uploaded successfully!"


@app.route("/logout")
def logout():
    session.pop('user')
    return redirect('/login')


@app.route("/dashboard", methods=['GET', 'POST'])
def dashboard():

    if 'user' in session and session['user'] == params['username'] :
        posts = Posts.query.all()
        return render_template('dashboard.html', params=params, posts=posts)

    if request.method == 'POST':
        username = request.form.get('uname')
        password = request.form.get('pass')
        if username == params['username'] and password == params['password']:
            session['user']= username
            posts = Posts.query.all()
            return render_template('dashboard.html', params=params, posts=posts)

    else:
        return render_template('login.html', params=params)

@app.route("/login", methods=['GET'])
def login_route():
    return render_template('login.html', params=params)


@app.route("/about")
def about_route():
    return render_template('about.html', params=params)

@app.route("/post/<string:post_slag>", methods=['GET'])
def post_route(post_slag):
    post = Posts.query.filter_by(slag=post_slag).first()

    return render_template('post.html', params=params, post=post)



@app.route("/contact", methods=['GET', 'POST'])
def contact_route():
    if request.method == 'POST':
        name = request.form.get('name')
        email = request.form.get('email')
        phone = request.form.get('phone')
        message = request.form.get('message')
        entry = Contacts(name = name, email = email , phone = phone, msg = message, date = datetime.now())
        db.session.add(entry)
        db.session.commit()

    return render_template('contact.html', params=params)


if __name__ == '__main__':
    app.run(debug=True)